// Soal 1


const luas = (panjang, lebar) => {
    let luas = panjang * lebar;

    return luas
}

const keliling = (panjang, lebar) => {
    const keliling = 2 *  (panjang + lebar);
    return keliling;
}


console.log(luas(2, 1))
console.log(keliling(5, 1))


// // Soal 2
// // Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName)
    }
  }
}
 
// Driver Code 
newFunction("William", "Imoh").fullName() 


// // Soal 3
// // Diberikan sebuah objek sebagai berikut:

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;


const {firstName, lastName, address, hobby} = newObject

// // Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
// // Driver code
console.log(firstName, lastName, address, hobby)


// // Soal 4
// // Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west, ...east]

//Driver Code
console.log(combined)

// Soal 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var after = `Lorem  ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before);
console.log(after);